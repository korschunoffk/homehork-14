# borg backup

Был поднят стенд - 2 VM Centos7

1. client 192.168.1.2           -- Машина с которой будем делать бэкапы
2. borgserver 192.168.1.1       -- Машина на которой будет храниться репозиторий с бэкапами

# Настройки Client

После ansible provision на client вручную был сгенерирован беспарольный ssh ключ ( `ssh-keygen -t rsa` ) и отправлен на borgserver (`ssh-copy-id root@borgserver`)
Так же в ручную на client был инициализирован репозиторий находящийся на borgserver `borg init -e none borgserver:client-etc` 

ansibl'ом автоматически была создана задача для cron  - 

`0 1 * * * /opt/borg-etc.sh 2>&1 | tee -a /var/log/borg`

Запускаем скрипт `borg-etc.sh` раз в час, stdout и stderr перенаправляем в лог-файл `/var/log/borg`


Для примера восстановления из бэкапа, на client была создана директория `/etc/testborg` и файл в ней `/etc/testborg/one`

1. в ручную был запущен скрипт borg-etc.sh который создал текущую резеврную копию `/etc`
2. был удален каталог `/etc/testborg`
3. просмотрен список доступных бэкапов `borg list root@borgserver:client-etc` и выбран необходимый
4. создана временная папка для монтирования бэкапа `mkdir /tmp/backup`
5. в нее монтирован последний бэкап `borg mount root@borgserver:client-etc::*backupname* /tmp/backup`
6. скопированы файлы `cp -R /tmp/backup/etc/testborg /etc/testborg`
7. отмонтирован бэкап `borg umount /tmp/backup`


# Создание бэкапа
для создания бэкапа в скрипте borg-etc.sh предназначена следующая строка

`borg create --stats --progress ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_REPO}::"etc-{now:%Y-%m-%d_%H:%M:%S}" /etc`

# Очистка бэкапов

`borg prune -v --list ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_REPO} --keep-within=1m --keep-monthly=2`

где
`--keep-within=1m` - сохраняем все бэкапы за последний месяц 
`--keep-monthly=2` -сохраняем последний бэкап месяца за два предыдущих месяца.

В репозитории приведен полный скрипт бэкапа `borg-etc.sh`, 
а так же в файле `screenlog.0` записана вышеописанная послеовательность действий по восстановлению из бэкапа.
